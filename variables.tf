variable "zone" {}
variable "project" {}
variable "ssh_private_key" {}
variable "ssh_user" {}
variable "instances" {}
variable "pki_role_name" {}
variable "pki_backend_path" {}
variable "datacenter" {}
variable "control_plane_consul_gossip_key" {}
variable "consul_version" { default = "" }
variable "cluster_name" {}
