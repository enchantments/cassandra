
#!/bin/bash

set -e

export DEBIAN_FRONTEND=noninteractive

sleep 15

sudo apt update -yq
sudo apt install -y \
    unzip \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common \
    jq

sync
