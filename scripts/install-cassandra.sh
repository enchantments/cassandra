#!/bin/bash

echo "deb http://www.apache.org/dist/cassandra/debian 310x main" | sudo tee -a /etc/apt/sources.list.d/cassandra.sources.list
curl -s https://www.apache.org/dist/cassandra/KEYS | sudo apt-key add -
sudo apt update
sudo apt upgrade -y
sudo apt install -y libjemalloc1 cassandra
sudo systemctl stop cassandra
sudo systemctl disable cassandra

# Remove the default configuration files
sudo rm -f /etc/init.d/cassandra
sudo rm -f /etc/cassandra/cassandra.yml
sudo rm -f /etc/cassandra/cassandra.yaml
sudo rm -f /etc/default/cassandra

# Gut out the sysv garbage.

sudo systemctl daemon-reload
sudo rm -f /lib/systemd/system/cassandra.service

# Enable the Consul seed provider
sudo curl --silent --location --output /usr/share/cassandra/cassandra-consul-seed-provider-1.2.3-all.jar https://github.com/karnauskas/cassandra-consul-seed-provider/releases/download/v1.2.3/cassandra-consul-seed-provider-1.2.3-all.jar
