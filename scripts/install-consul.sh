#!/bin/bash

set -e

. /tmp/hashicorp-product-versions.env

gpg --recv-keys 51852D87348FFC4C

curl -Os https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_SHA256SUMS
curl -Os https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_SHA256SUMS.sig
curl -Os https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip
gpg --verify consul_${CONSUL_VERSION}_SHA256SUMS.sig consul_${CONSUL_VERSION}_SHA256SUMS
sha256sum --check --ignore-missing consul_${CONSUL_VERSION}_SHA256SUMS
sudo unzip -qq consul_${CONSUL_VERSION}_linux_amd64.zip -d /usr/bin
rm -f consul_${CONSUL_VERSION}_SHA256SUMS consul_${CONSUL_VERSION}_SHA256SUMS.sig consul_${CONSUL_VERSION}_linux_amd64.zip

sudo tee -a /etc/environment <<EOF
CONSUL_HTTP_ADDR=https://localhost:8501
CONSUL_CACERT=/etc/tls/ca.pem
CONSUL_CLIENT_KEY=/etc/tls/key.pem
CONSUL_CLIENT_CERT=/etc/tls/cert.pem
EOF

consul version
sync
