resource "google_compute_disk" "commit_log" {
  count   = var.instances
  name    = "cassandra-${count.index}-commitlog"
  type    = "pd-ssd"
  project = var.project
  zone    = var.zone
  size    = 40
}

resource "google_compute_disk" "data" {
  count   = var.instances
  name    = "cassandra-${count.index}-data"
  type    = "pd-ssd"
  project = var.project
  zone    = var.zone
  size    = 250
}
