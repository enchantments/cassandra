resource "google_compute_instance" "node" {
  count = var.instances

  name         = "cassandra-${count.index + 1}"
  machine_type = "n1-standard-2"
  project      = var.project
  zone         = var.zone

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }

  attached_disk {
    device_name = "commitlog"
    source      = google_compute_disk.commit_log[count.index].self_link
  }

  attached_disk {
    device_name = "data"
    source      = google_compute_disk.data[count.index].self_link
  }

  network_interface {
    network = "default"

    access_config {
    }
  }

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }

  connection {
    type        = "ssh"
    host        = self.network_interface.0.access_config.0.nat_ip
    user        = var.ssh_user
    private_key = file(var.ssh_private_key)
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /tmp/tls /tmp/cassandra",
      "sudo mkdir -p /etc/tls /etc/consul",
    ]
  }

  provisioner "file" {
    destination = "/tmp/tls/key.pem"
    content     = tls_private_key.node[count.index].private_key_pem
  }

  provisioner "file" {
    destination = "/tmp/tls/ca.pem"
    content     = vault_pki_secret_backend_sign.node[count.index].ca_chain[length(vault_pki_secret_backend_sign.node[count.index].ca_chain) - 1]
  }

  provisioner "file" {
    destination = "/tmp/tls/cert.pem"
    content = join("\n", flatten([
      vault_pki_secret_backend_sign.node[count.index].certificate,
      vault_pki_secret_backend_sign.node[count.index].ca_chain,
    ]))
  }

  provisioner "file" {
    destination = "/tmp"
    source      = "${path.module}/files"
  }

  provisioner "file" {
    destination = "/tmp/files/consul/datacenter.json"
    content = templatefile("${path.module}/templates/consul/datacenter.json", {
      datacenter = var.datacenter
    })
  }

  provisioner "file" {
    destination = "/tmp/files/consul/gossip.json"
    content = templatefile("${path.module}/templates/consul/gossip.json", {
      gossip_key = var.control_plane_consul_gossip_key
    })
  }

  provisioner "file" {
    destination = "/tmp/files/consul/retry-join.json"
    content = templatefile("${path.module}/templates/consul/retry-join.json", {
      project = var.project
    })
  }

  provisioner "file" {
    destination = "/tmp/files/consul/cassandra.json"
    content     = file("${path.module}/files/consul/cassandra.json")
  }

  provisioner "file" {
    destination = "/tmp/hashicorp-product-versions.env"
    content = join("\n", [
      "CONSUL_VERSION=${coalesce(var.consul_version, jsondecode(data.http.consul_product_info.body).current_version)}",
    ])
  }

  provisioner "remote-exec" {
    scripts = [
      "${path.module}/scripts/install-basic-software.sh",
      "${path.module}/scripts/install-consul.sh",
      "${path.module}/scripts/install-cassandra.sh",
    ]
  }

  provisioner "file" {
    destination = "/tmp/files/cassandra/cassandra.yaml"
    content = templatefile("${path.module}/templates/cassandra/cassandra.yaml", {
      listen_address = self.network_interface.0.network_ip,
      seed           = self.network_interface.0.network_ip,
      cluster_name   = var.cluster_name
    })
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/tls/* /etc/tls",
      "sudo mv /tmp/files/consul/* /etc/consul",
      "sudo mv /tmp/files/systemd/* /lib/systemd/system",
      "sudo mv /tmp/files/cassandra/cassandra /etc/default/cassandra",
      "sudo mv /tmp/files/cassandra/cassandra.yaml /etc/cassandra/cassandra.yaml",
      "sudo systemctl daemon-reload",
      "sudo systemctl enable consul.path",
      "sudo systemctl start consul.path",
      "sleep 20",
      "consul kv put cassandra/seeds/${self.network_interface.0.network_ip}",
      "sudo systemctl enable cassandra.path",
      "sudo systemctl start cassandra.path",
    ]
  }

  provisioner "remote-exec" {

    when = destroy

    inline = [
      "consul kv delete cassandra/seeds/${self.network_interface.0.network_ip}",
      "sudo systemctl disable cassandra.path",
      "sudo systemctl stop cassandra.path",
      "sudo systemctl disable cassandra.service",
      "sudo systemctl stop cassandra.service",
      "sudo systemctl disable consul.path",
      "sudo systemctl stop consul.path",
      "sudo systemctl disable consul.service",
      "sudo systemctl stop consul.service",
    ]
  }
}
