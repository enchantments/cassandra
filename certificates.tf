resource "vault_pki_secret_backend_sign" "node" {
  count = var.instances

  backend = var.pki_backend_path
  name    = var.pki_role_name

  csr         = tls_cert_request.node[count.index].cert_request_pem
  common_name = tls_cert_request.node[count.index].subject[0].common_name
}

resource "tls_private_key" "node" {
  count = var.instances

  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

resource "tls_cert_request" "node" {
  count = var.instances

  key_algorithm   = "ECDSA"
  private_key_pem = tls_private_key.node[count.index].private_key_pem

  subject {
    common_name = "control-plane"
  }

  dns_names = flatten([
    "localhost",
    formatlist("%s.service.consul", ["cassandra"]),
  ])

  ip_addresses = [
    "127.0.0.1",
  ]
}
